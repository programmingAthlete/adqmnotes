Perturbation theory in Quantum and Statistical mechanics
========================================================
\tableofcontents

## Perturbation theory for the Schrödinger equation

### Variation of the constant (projection on a basis of functions)

### Alternative derivation: Dyson series 

### 1st order time-independent perturbation theory

### 2nd order time-independent perturbation theory

### Applications: Recap of the Hydrogen atom

### Applications: Stark effect

### Applications: Zeeman effect

### References


## Perturbation theory for the Liouville-Von Neumann equation

## Recap of mixed state quantum dynamics

### Quantum Green-Kubo response functions

### A Markovian equation for the evolution of the density matrix

### Markov processes, equilibrium distributions and Gibbs states

### Decoherence and response functions: role of the bath and of the size of the system

### Application: quantum Johnson-Nyquist noise

### References


## Response functions using the path integral formulation

### Evolution equation for stochastic processes: jumps and diffusion

### Path integral for diffusion and Green-Kubo furmula - Gaussian noise

### Path integral for jump process - Poisson noise

### Application: Green-Kubo formula for a single quantum dot

### Application: Arrhenius rate from path integral

### References

